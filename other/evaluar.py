#!/usr/bin/python3
import subprocess as sp
import sys
import os
import glob
import re
from pathlib import Path

def uso():
    print(f"Uso: {sys.argv[0]} [dir_practica] [dir_diffs]", file=sys.stderr)
    sys.exit(1)

def runPractica(cmd, **kwargs):
    return sp.run(cmd, cwd=dir_practica, capture_output=True, **kwargs)

def testResults(testOutput):
    reResult = r"Tests run:\s(?P<totalRuns>\d+),\sFailures:\s(?P<failures>\d+),\sErrors:\s(?P<errors>\d+)"
    reTime = r"Total time:\s*(?P<time>\d+\.\d+)\ss"

    result = re.findall(reResult, testOutput)
    if result:
        result = result[-1]
    else:
        result = ('0', '0', '0')
    time = re.findall(reTime, testOutput)[-1]

    return (result[0], result[1], result[2], time)

if __name__ == '__main__':

    args = sys.argv[1:]

    if not args:
        uso()

    dir_practica = args[0]
    dir_diffs = args[1]

    dir_practica = os.path.abspath(dir_practica)
    dir_diffs = os.path.abspath(dir_diffs)

    # Ir a rama test y descartar cambios
    runPractica(['git', 'checkout', 'master'])
    runPractica(['git', 'checkout', '-B', 'test'])
    runPractica(['git', 'reset', '--hard', 'HEAD'])

    results = []


    for diff_file in glob.glob(f"{dir_diffs}/*.diff"):
        diff = Path(diff_file).stem

        # Aplicar parche
        with open(diff_file) as f:
            patch_result = runPractica(['patch', '-p1'], stdin=f)

        # Revisar si modificaron las pruebas y descartar los cambios
        patch_output = patch_result.stdout.decode('utf-8')
        if re.findall(r'test', patch_output):
            print(f"Pruebas modificadas: {diff}", file=sys.stderr)
            runPractica(['git', 'checkout', 'src/test'])

        # Correr las pruebas y sacar los resultados
        test_output = runPractica(['mvn', 'test'])
        result = testResults(test_output.stdout.decode('utf-8'))
        results.append(result)
        print(f"{diff},{','.join(result)}")


        # Regresar a commit de prueba
        runPractica(['git', 'reset', '--hard', 'HEAD'])
        runPractica(['git', 'clean', '-xfd', '.'])

