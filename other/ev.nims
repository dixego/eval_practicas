import std/[tables,sugar,osproc,nre,strutils], os, strformat

if paramCount() < 2 or paramCount() > 2:
  echo("Se necesita el directorio de la práctica y de los diferenciales")
  quit(1)

let
  practica = paramStr(1).absolutePath()
  diffs = paramStr(2).absolutePath()
  resultRe: Regex = re"Tests run:\s(?P<totalRuns>\d+),\sFailures:\s(?P<failures>\d+),\sErrors:\s(?P<errors>\d+)"
  timeRe: Regex = re"Total time:\s*(?P<time>\d+\.\d+)\ss"

template qx(s: string): untyped =
  execCmdEx(s, workingDir = practica) # no such thing as a bad syntax extension
template err(s: string): untyped =
  stderr.writeLine(s)
type TestResult =
  tuple[cuenta: string, pruebas: int, fallos: int, errores: int, tiempo: string]
proc reset() =
  discard(qx"git reset --hard HEAD")

func collectNamedMatches(s: string, pattern: Regex): seq[Table[string, string]] =
  result = collect:
    for match in s.findAll(pattern):
      match.find(pattern).get().captures().toTable()

proc patchAndTest(diff: string): TestResult =
  reset()
  let (_, cuenta, _) = diff.splitFile()
  result = (cuenta, 0, 0, 0, "0.0")

  var (output, code) = qx(fmt"patch -p1 -i {diff}")
  if code != 0:
    err(&"Falló en aplicarse el parche: {cuenta}")
    return

  discard(qx"git checkout src/test")

  (output, code) = qx"mvn test"
  let matches = output.collectNamedMatches(resultRe)
  if matches.len == 0:
    err(&"Fallo de compilación: {cuenta}")
    return
  let match = matches[^1]
  let time = output.collectNamedMatches(timeRe)[0]["time"]

  result = (cuenta, match["totalRuns"].parseInt(), match["failures"].parseInt(), match["errors"].parseInt(), time)

proc main() =
  discard(qx"git checkout -B test")
  var results: seq[TestResult] = @[]
  for diff in (&"{diffs}/*.diff").walkFiles():
    err(&"Probando {diff}")
    results.add(patchAndTest(diff))

  reset()
  echo("Cuenta,Calificacion,Comentario,Pruebas,Fallos,Errores,Tiempo")
  for it in results:
    echo(&"{it.cuenta},,,{it.pruebas},{it.fallos},{it.errores},{it.tiempo}")

when isMainModule:
  main()
