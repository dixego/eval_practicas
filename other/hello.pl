#!/usr/bin/perl
use strict;
use warnings;

use Cwd 'abs_path';
use File::Basename;

my ($practica, $diffsDir) = @ARGV;

if (not defined $practica or not defined $diffsDir) {
  die "Se necesita la práctica y la carpeta de diffs";
}

$practica = abs_path($practica);
$diffsDir = abs_path($diffsDir);

my @diffs = glob($diffsDir . "/*.diff");

chdir $practica;
qx{git checkout -B test 2>/dev/null};

print "Cuenta,Calificacion,Comentario,Pruebas,Fallos,Errores,Tiempo\n";

for my $diff (@diffs) {
  qx{git reset --hard HEAD 2> /dev/null};
  qx{patch -p1 -i $diff 2> /dev/null};
  next if ($? != 0);

  my ($cuenta, $dir, $ext) = fileparse($diff, qr/\.[^.]*/);

  my $testOutput = qx{mvn test 2> /dev/null};

  if ($? != 0) {
    print "$cuenta,,,0,0,0,0.0\n";
    next;
  }

  my $testRe = 'Tests run:\s(?P<totalRuns>\d+),\sFailures:\s(?P<failures>\d+),\sErrors:\s(?P<errors>\d+)';
  my $timeRe = 'Total time:\s*(?P<time>\d+\.\d+)\ss';

  my @testMatches = $testOutput =~ /$testRe/g;
  my @lastMatch = @testMatches[-3..-1];

  my $time = $testOutput =~ /$timeRe/g;
  
  print "$cuenta,,,$lastMatch[0],$lastMatch[1],$lastMatch[2],$1\n";

}

qx{git reset --hard HEAD 2> /dev/null};
