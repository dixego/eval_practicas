Programa para evaluar las prácticas de ICC/EDD, escrito en Rust por razones de ocio.

En [other](./other) se encuentran versiones alternativas para realizar la misma tarea, en
Python y Perl.
