use regex::{Captures, Regex};

#[derive(Debug)]
pub struct TestResult {
    pub cuenta: String,
    pub pruebas: i32,
    pub fallos: i32,
    pub errores: i32,
    pub tiempo: String,
}

impl TestResult {
    pub fn serialize(&self) {
        println!(
            "{},{},{},{},{},{},{}",
            self.cuenta, "", "", self.pruebas, self.fallos, self.errores, self.tiempo
        );
    }

    pub fn default_test_result() -> TestResult {
        TestResult {
            cuenta: "".to_string(),
            pruebas: 0,
            fallos: 0,
            errores: 0,
            tiempo: "0.0".to_string(),
        }
    }

    pub fn from_mvn_output(output: String) -> TestResult {
        let tests_re = Regex::new(r"Tests run:\s(?P<totalRuns>\d+),\sFailures:\s(?P<failures>\d+),\sErrors:\s(?P<errors>\d+)").unwrap();
        let time_re = Regex::new(r"Total time:\s*(?P<time>\d+\.\d+)\ss").unwrap();

        let tests_captures = tests_re
            .captures_iter(output.as_str())
            .collect::<Vec<Captures>>();
        let time_captures = time_re
            .captures_iter(output.as_str())
            .collect::<Vec<Captures>>();

        if let Some(last_test) = tests_captures.last() {
            TestResult {
                cuenta: "".to_string(),
                pruebas: last_test
                    .name("totalRuns")
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                fallos: last_test
                    .name("failures")
                    .unwrap()
                    .as_str()
                    .parse()
                    .unwrap(),
                errores: last_test.name("errors").unwrap().as_str().parse().unwrap(),
                tiempo: time_captures
                    .last()
                    .unwrap()
                    .name("time")
                    .unwrap()
                    .as_str()
                    .to_string(),
            }
        } else {
            TestResult::default_test_result()
        }
    }
}

#[cfg(test)]
mod tests {
    use regex::Regex;

    #[test]
    fn re_test() {
        let re = Regex::new(r"(?P<number>\d{4})").unwrap();
        let re2 = Regex::new(r"(?P<number>\d{2})").unwrap();
        let text = "hello 1234 goobye 12";

        let captures = re.captures(text).unwrap();
        assert_eq!("1234", captures.name("number").unwrap().as_str());

        let captures2 = re2.captures(text).unwrap();
        assert_eq!("12", captures2.name("number").unwrap().as_str());

        for caps in re2.captures_iter(text) {
            println!("capture: {}", &caps["number"]);
        }
    }
}

pub mod shell {
    use super::TestResult;
    use glob::glob;
    use std::path::{Path, PathBuf};
    use std::process::Command;
    use std::str;

    /// regresa los nombres de los archivos .diff presentes en `dir`.
    pub fn ls_diffs(dir: &Path) -> Vec<PathBuf> {
        glob(&format!("{}/*.diff", dir.to_str().unwrap()))
            .expect("Failed to read glob pattern")
            .map(|res| res.unwrap().canonicalize().unwrap())
            .collect()
    }

    /// crea y cambia a la rama `test` en `dir` y la resetea
    pub fn git_reset(dir: &Path) {
        let _checkout = Command::new("git")
            .args(["checkout", "-B", "test"])
            .current_dir(dir.to_str().unwrap())
            .output()
            .expect("Failed to checkout to test");

        let _reset = Command::new("git")
            .args(["reset", "--hard", "HEAD"])
            .current_dir(dir.to_str().unwrap())
            .output()
            .expect("Failed to reset");
    }

    /// aplica el parche situado en `diff` al directorio en `dir`
    pub fn patch(dir: &Path, diff: &Path) -> Result<(), ()> {
        let exit_code = Command::new("patch")
            .args(["-p1", "-i", diff.to_str().unwrap()])
            .current_dir(dir.to_str().unwrap())
            .output()
            .expect("Failed to patch");

        let _checkout_test = Command::new("git")
            .args(["checkout", "src/test"])
            .current_dir(dir.to_str().unwrap())
            .output()
            .expect("Failed to checkout src/test");

        match exit_code.status.success() {
            true => Ok(()),
            false => Err(()),
        }
    }

    /// ejecuta `mvn test` y extrae los resultados de las pruebas
    pub fn mvn_test(dir: &Path) -> TestResult {
        let test_output = Command::new("mvn")
            .arg("test")
            .current_dir(dir.to_str().unwrap())
            .output()
            .expect("Failed to mvn test");

        TestResult::from_mvn_output(str::from_utf8(&test_output.stdout).unwrap().to_string())
    }
}
