mod lib;
use lib::*;

use std::env;
use std::path::PathBuf;

fn uso() {
    println!("Uso: eval_practicas [DIR_PRACTICA] [DIR_DIFFS]");
}

fn main() {
    let args = env::args().collect::<Vec<String>>();

    if args.len() != 3 {
        uso();
        std::process::exit(1);
    }

    // Step one: get the diffs' paths
    let dir_practica = PathBuf::from(&args[1]).canonicalize().unwrap();
    let dir_diffs = PathBuf::from(&args[2]).canonicalize().unwrap();

    let diffs: Vec<PathBuf> = shell::ls_diffs(dir_diffs.as_path());

    // there was a Step Two here. it's gone now.

    let mut results: Vec<TestResult> = Vec::new();

    // Step three: apply patches, run tests, collect results
    for diff in diffs {
        let cuenta: String = diff.file_stem().unwrap().to_str().unwrap().into();

        // three.1: reset
        shell::git_reset(dir_practica.as_path());

        // three.2: patch
        let patch_res = shell::patch(dir_practica.as_path(), diff.as_path());

        if patch_res.is_err() {
            let mut def_res = TestResult::default_test_result();
            def_res.cuenta = cuenta;
            results.push(def_res);
            continue;
        }

        // three.3: test
        let mut test_result = shell::mvn_test(dir_practica.as_path());
        test_result.cuenta = cuenta;

        results.push(test_result);
    }

    shell::git_reset(dir_practica.as_path());

    println!("Cuenta,Calificacion,Comentario,Pruebas,Fallos,Errores,Tiempo");
    results.iter().for_each(|r| r.serialize());
}
